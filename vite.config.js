const { resolve } = require('path');
import { defineConfig, loadEnv } from 'vite'
import pageData from './index'

// https://vitejs.dev/config/
export default ({ mode }) => {
    process.env = { ...process.env, ...loadEnv(mode, process.cwd()) };
    let aliases = {}
    return defineConfig({
        build: {
            lib: {
                entry: resolve(__dirname, './index.js'),
                name:"index"
            },
        },
        resolve: {
            extensions: ['.js'],
            alias: aliases,
        },
        plugins: [
            pageData({
                varName: 'scpRequestData'//页面全局变量名
            }),
        ]
    })

};
